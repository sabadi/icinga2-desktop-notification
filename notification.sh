#!/bin/sh
file=/tmp/test

while true
do

	if tail $file | grep  "command"
	then

		if tail -2 $file | head -1 | jq ".host, .service, .check_result.output, .check_result.state"
		then 

        		host=$(tail -2 $file | head -1 | jq ".host")
        		service=$(tail -2 $file | head -1 | jq ".service")
        		check_result=$(tail -2 $file | head -1 | jq ".check_result.output")
        		state=$(tail -2 $file | head -1 | jq ".check_result.state")

			notify-send "Icinga Notification"  "Host: $host\nService: $service\nCheck_result: $check_result\nState: $state" --icon=dialog-information

		fi

		> /tmp/test
	fi

done

