# Icinga2 Desktop Notification

In this repository, i tried to write a simple bash scrip to produce desktop notification for icinga2. After runnig the scripts, you will receive desktop notification for services with critical status. The notification contains the host name, service, check result and the state of service.

# How you can test it

First query.sh script must be run in background for API-querys.

$ sudo bash query.sh&

Now we can run notification.sh script. If there are some critical services, you will see desktop notification.

$ sudo bash notification.sh
